import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { IPizza } from "src/interfaces";

export interface IPizzasState {
	pizzas: IPizza[];
	loadingPizzas: boolean;
}

const initialState: IPizzasState = {
	pizzas: [],
	loadingPizzas: false,
};

const pizzasSlice = createSlice({
	name: "pizzas",
	initialState,
	reducers: {
		createPizza: (state, action: PayloadAction<{ pizza: IPizza }>) => {
			const { pizza } = action.payload;
			state.pizzas = [...state.pizzas, pizza];
			state.loadingPizzas = false;
		},
		setPizzas: (state, action: PayloadAction<{ pizzas: IPizza[] }>) => {
			const { pizzas } = action.payload;
			state.pizzas = pizzas;
			state.loadingPizzas = false;
		},
		updatePizza: (state, action: { payload: { pizza: IPizza } }) => {
			const { pizza } = action.payload;
			const index: number = state.pizzas.findIndex((p: IPizza) => p._id === action.payload.pizza._id);
			state.pizzas[index] = pizza;
			state.loadingPizzas = false;
		},
		deletePizza: (state, action: { payload: { pizza: IPizza } }) => {
			const { pizza } = action.payload;
			const pizzas = state.pizzas.filter((p) => p._id !== pizza._id);
			state.pizzas = pizzas;
			state.loadingPizzas = false;
		},
		setPizzasLoading: (state, action: PayloadAction<boolean>) => {
			state.loadingPizzas = action.payload;
		},
	},
});

export const { createPizza, setPizzasLoading, setPizzas, updatePizza, deletePizza } = pizzasSlice.actions;
export default pizzasSlice;
