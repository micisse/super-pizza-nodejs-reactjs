import { PayloadAction, createSlice } from "@reduxjs/toolkit";

export interface IAppState {
	loading: boolean;
}

const initialState: IAppState = {
	loading: false,
};

const appSlice = createSlice({
	name: "app",
	initialState,
	reducers: {
		updateAppLoading: (state, action: PayloadAction<boolean>) => {
			state.loading = action.payload;
		},
	},
});

export const { updateAppLoading } = appSlice.actions;
export default appSlice;
